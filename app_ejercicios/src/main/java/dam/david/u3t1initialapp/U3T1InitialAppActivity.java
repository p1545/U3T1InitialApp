package dam.david.u3t1initialapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class U3T1InitialAppActivity extends AppCompatActivity implements OnClickListener {
    
    //Atributos
    private int count;
    private TextView tvDisplayResultado;
    private final String TV_DISPLAY_RESULTADO = "tvDisplayResultado";

    //TODO 1.- Añade dos botones más que nos permitan incrementar y decrementar en 2 unidades el número de elementos.

    private Button buttonIncrease, buttonDecrease, buttonReset, buttonIncreaseTwo, buttonDecreaseTwo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3_t1_initial_app);

        setUI();
    }

    //enlazando a recursos
    private void setUI() {

        //TODO 2.- Añade todo el código necesario para la gestión de los nuevos botones

        tvDisplayResultado = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonIncreaseTwo = findViewById(R.id.buttonIncreaseTwo);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonDecreaseTwo = findViewById(R.id.buttonDecreaseTwo);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonIncreaseTwo.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonDecreaseTwo.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonIncrease:
                count++;
                Log.i(LogActivity.DEBUG_TAG, "Incrementar elemento");
                break;
            case R.id.buttonDecrease:
                count--;
                Log.i(LogActivity.DEBUG_TAG, "Decrementar elemento");

                break;
            case R.id.buttonIncreaseTwo:
                count+=2;
                Log.i(LogActivity.DEBUG_TAG, "Incrementar dos elementos");
                break;
            case R.id.buttonDecreaseTwo:
                count-=2;
                Log.i(LogActivity.DEBUG_TAG, "Decrementar dos elementos");
                break;
            case R.id.buttonReset:
                count = 0;
                Log.i(LogActivity.DEBUG_TAG, "Resetear elementos");
                break;
        }
        //Mostrar valor de cuenta de tvDisplay
        tvDisplayResultado.setText(getString(R.string.number_of_elements) + ": " + count);
    }

    //TODO 7.- Modifica el código para salvar y recuperar la información del textView al cambiar la orientación del dispositivo.

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(TV_DISPLAY_RESULTADO, tvDisplayResultado.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState != null) {
            tvDisplayResultado.setText(savedInstanceState.getString(TV_DISPLAY_RESULTADO));
        }
    }

    //TODO 3.- Rediseña el layout: Sustituye todos los LinearLayout por un constraint_layout tanto para la versión landscape como la versión portrait.
    // Organiza los botones de la forma más adecuada. Haz uso de chains y guidelines.

}