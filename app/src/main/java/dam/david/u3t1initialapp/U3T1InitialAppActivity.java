package dam.david.u3t1initialapp;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class U3T1InitialAppActivity extends AppCompatActivity implements OnClickListener {
    
    //Atributos
    private int count;
    private TextView tvDisplayResultado;

    private Button buttonIncrease, buttonDecrease, buttonReset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_u3_t1_initial_app);

        setUI();
    }

    //enlazando a recursos
    private void setUI() {

        tvDisplayResultado = findViewById(R.id.tvDisplay);
        buttonIncrease = findViewById(R.id.buttonIncrease);
        buttonDecrease = findViewById(R.id.buttonDecrease);
        buttonReset = findViewById(R.id.buttonReset);

        buttonIncrease.setOnClickListener(this);
        buttonDecrease.setOnClickListener(this);
        buttonReset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonIncrease:
                count++;
                Log.i(LogActivity.DEBUG_TAG, "Incrementar elemento");
                break;
            case R.id.buttonDecrease:
                count--;
                Log.i(LogActivity.DEBUG_TAG, "Decrementar elemento");
                break;
            case R.id.buttonReset:
                count = 0;
                Log.i(LogActivity.DEBUG_TAG, "Resetear elementos");
                break;
        }
        //Mostrar valor de cuenta de tvDisplay
        tvDisplayResultado.setText(getString(R.string.number_of_elements) + ": " + count);
    }
}