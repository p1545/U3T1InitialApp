package dam.david.u3t1initialapp;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

public class SplashActivity extends LogActivity {

    private MediaPlayer sound;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //TODO 6.- - La barra de título no debe mostrarse en la activity Splash.
        // a) ocultala
        // b) mostrarla a pantalla completa.

        requestWindowFeature(Window.FEATURE_NO_TITLE); //Para esconder el título
        getSupportActionBar().hide(); //Para esconder la barra de título
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN); //Activar pantalla completa

        setContentView(R.layout.activity_splash);

        //Esperamos 500ms para lanzar el audio y establecemos la duración del splash a 5,5 segundos para dar tiempo a finalizar el audio
        Thread timer = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(500);
                    sound = MediaPlayer.create(getApplicationContext(), R.raw.sound);
                    sound.start();
                    sleep(5500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                finally {
                    startActivity(new Intent("dam.david.U3T1InitialApp.STARTINGPOINT"));
                }
            }
        };
        //Iniciamos el thread
        timer.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //Liberamos recursos
        sound.release();
        //Terminamos la aplicación
        finish();
    }

    @Override
    protected void onResume() {

        super.onResume();
    }

    //TODO 4.- Bloquea la orientación a “portrait” para que no se cree un nuevo thread cada vez que se produce un cambio de orientación.
    // (Definido en el manifest)

    //TODO 5.- - Genera la colección de imágenes del fondo para que se vea correctamente para las
    // diferentes densidades de dispositivos. Copia los directorios generados al lugar adecuado dentro de recursos
}

